#use jdk8 alpine base image
FROM openjdk:8-jdk-alpine

#add the spring boot jar
COPY target/spring-boot-0.0.1-SNAPSHOT.jar /opt/spring-boot-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/opt/spring-boot-0.0.1-SNAPSHOT.jar"]
